// ============================================================================
//
// = CONTEXT
//    TANGO Project -
//
// = FILENAME
//    PicoScaleTaskManager.hpp
//
// = AUTHOR
//    F. Thiam
//
// ============================================================================

#ifndef _MY_DEVICE_TASK_H_
#define _MY_DEVICE_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/utils/XString.h>
#include <SmarActSI.h>
#include <SmarActSIConstants_PS.h>
#include "Utils.h"
#include <bitset>  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#define DEFAULT_TIMEOUT 0xBB8
#define MIN_TIMEOUT 1

const int CHANNEL_1 = 1;
const int CHANNEL_2 = 2;
const int CHANNEL_3 = 3;
const int CHANNEL_1_Index = 0;
const int CHANNEL_2_Index = 1;
const int CHANNEL_3_Index = 2;
const int POSITION_ZERO = 0;
const bool SUCCESS = true;
const bool FAILURE = false;
const int MAX_LENGTH        = 100000;
const int MAX_BUFFER_SIZE   = 220;
const int DEFAULT_PACK_SIZE = 100;
const int NONE = 0;
const int MODERATE = 1;
const int FATAL = 2;
const int NUMBER_OF_CHANNELS = 2; // '(3) starts with 0...
const int STREAM_BUFFER_ID = 3;
const int FRAME_SIZE = 8;
const int MAX_CHANNEL_BUFFER_SIZE = 10000;
const std::string ZERO_STRING_VALUE ="1111111111111111111111111111111111111111111111111111111111111111";
// ============================================================================
// class: PicoScaleTaskManager
// ============================================================================
class PicoScaleTaskManager : public yat4tango::DeviceTask
{
public:

  //- Initialization 
  PicoScaleTaskManager (Tango::DeviceImpl * devHost, std::string deviceID);

  //- the Tango device hosting this task
  Tango::DeviceImpl * _hostDev;
  
	//- dtor
  virtual ~PicoScaleTaskManager (void);
  
	typedef struct {
		double positionsBufferedV1[MAX_LENGTH];
		double positionsBufferedV2[MAX_LENGTH];
		double positionsBufferedV3[MAX_LENGTH];
		double sampleQuantityBuffered;
	}dataPacket;
  
  typedef struct {
		int sampleQuantity;   
    int acquisitionFrequency;
	}acquisitionParams;

  union Converter { uint64_t i; double d; };
  
  void resetPositions();
  void startAcquisition(int sampleQuantity,  int acquisitionFrequency);
  void stopAcquisition();
	
  double* getData(int channel);
  double getSampleQuantityBuffered();
  
  void dataImported();
  bool isAcquisitionOver();
  Tango::DevState getState();
  std::string getStatus();
  
protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg)
    throw (Tango::DevFailed);
	//Mutex for data access//dll
	yat::Mutex _data_mutex;
	yat::Mutex _pS_Access;

private:
	//Connection 	
  SA_SI_Handle _psHandler;
  
  //char *convert (int a);
  bool _connected;
  
	//Config
  void configureAcquisition(int acquisitionFrequency);
	//State
  Tango::DevState _state;
  std::string _status;
  bool _streamingAcquisitionRunning;
  int _numberOfSamplesNeeded;
  void setState(Tango::DevState state);
	void setStatus(std::string state);
  void checkQuantityBuffered();
  void refreshSignalQuality();
  
  void checkTaskState();
  
	//Data transfers
  dataPacket _dataToReturn;	
	bool _acquisitionOver;
  
	//pS access
  void initPS(std::string deviceID);
  void closePS();
  int resetStreamingPS();
  bool startStreamingPS();
  void stopStreamingPS();
  bool geChannelStatePS(int channel);
  int setProperty32PS(SA_SI_PropertyKey propertyToSet, int value);
  int setProperty64PS(SA_SI_PropertyKey propertyToSet, int value);
  bool waitAcqForBufferPS(int gravity, unsigned int timeOut, unsigned int *bufferID);
  void getBufferFromPS(unsigned int bufferID);
  //Check PS state
  bool checkResultFromAPI(int result, int severity, std::string description);
  std::string getMessage(int code);
  
  void extractBuffer(uint8_t* dest, uint32_t frameNumber, double *channelToRecord);


  void configurePicoscale();

  double _oldFrequency;
};

#endif // _MY_DEVICE_TASK_H_
