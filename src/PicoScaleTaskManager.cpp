// ============================================================================
//
// = CONTEXT
//    TANGO Project -
//
// = FILENAME
//    PicoScaleTaskManager.cpp
//
// = AUTHOR
//    F. Thiam
//
// ============================================================================
// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PicoScaleTaskManager.h"
#define SAMPLE_TIME_UNIT 1.024e-5 

static const unsigned short kPERIODIC_PERIOD_MS = 10;
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kSTART_STREAMING_ACQUISITION  		(yat::FIRST_USER_MSG + 1006)
// ============================================================================
// PicoScaleTaskManager::PicoScaleTaskManager
// ============================================================================
PicoScaleTaskManager::PicoScaleTaskManager (Tango::DeviceImpl * host_dev, std::string deviceID)
:	yat4tango::DeviceTask(host_dev),
_hostDev(host_dev)
{
  //Initialize member variables
  yat::MutexLock gardDataMutex(this->_data_mutex);
  yat::MutexLock gardpSAccessMutex(this->_pS_Access);
  for (unsigned int i = 0; i < MAX_LENGTH; ++i ){
      this->_dataToReturn.positionsBufferedV1[i] = 0;
      this->_dataToReturn.positionsBufferedV2[i] = 0;
      this->_dataToReturn.positionsBufferedV3[i] = 0;
    } 
  this->_oldFrequency = -1;
  this->_dataToReturn.sampleQuantityBuffered = 0;
  this->_psHandler = 0;
  this->_connected = false;
  //INITIALIZE SMARACT INTERFEROMETER 
  this->initPS(deviceID);
}//release Data/Dll access mutex
// ============================================================================
// PicoScaleTaskManager::~PicoScaleTaskManager
// ============================================================================
PicoScaleTaskManager::~PicoScaleTaskManager ()
{
  _psHandler = 0;
	this->enable_periodic_msg(false);
  this->closePS();
	this->_connected = false;
	yat::MutexLock gard(this->_pS_Access);
}

// ============================================================================
// PicoScaleTaskManager::process_message
// ============================================================================
void PicoScaleTaskManager::process_message (yat::Message& _msg)
  throw (Tango::DevFailed)
{
  //DEBUG_STREAM << "PicoScaleTaskManager::process_message::receiving msg " <<_msg.to_string() << std::endl;
  //- handle msg
  switch (_msg.type())
  {
     //- TASK_PERIODIC ------------------
    case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "PicoScaleTaskManager::handle_message::TASK_PERIODIC" << std::endl;
      if(_connected){
        // If acquisition is running, periodic will wait until a buffer can be read (blocking operation)
        // Will do this operation until sample quantity is reached...
        if(_streamingAcquisitionRunning){
          this->_state = Tango::RUNNING;
          this->setStatus("Device streaming data from interferometer" );
          unsigned int bufferID = 0;
          while (_streamingAcquisitionRunning){ 
            //Will block until a buffer arrives
            bool response = this->waitAcqForBufferPS(NONE, DEFAULT_TIMEOUT, &bufferID); 
            if (response){
              //Get buffer, extract data, notify that data has been updated
              this->getBufferFromPS(bufferID); 
              //Check if quantity of data buffered is enough (stops acq if it is)
              checkQuantityBuffered();
            }
          }
          this->_acquisitionOver = true;
        }
        else{
          refreshSignalQuality();
        }
        
      }
      else{
        this->setState(Tango::FAULT);
        this->setStatus("Device is not connected, check ID property and try to init." );
      }
    }
    break;

    //- TASK_INIT ----------------------
    case yat::TASK_INIT:
    {
      DEBUG_STREAM << "PicoScaleTaskManager::handle_message::TASK_INIT::thread is starting up" << std::endl;
      this->enable_timeout_msg(false);
      this->set_periodic_msg_period(kPERIODIC_PERIOD_MS);
      this->enable_periodic_msg(true);

      //Configuration acquisition..
       this->configurePicoscale();
      this->_state = Tango::STANDBY;
      this->setStatus("Device is in idle state" );
    } 
    break;

    //- TASK_EXIT ----------------------
    case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "PicoScaleTaskManager::handle_message::TASK_EXIT::thread is quitting" << std::endl;
    }
    break;

    //- THREAD_TIMEOUT -------------------
    case yat::TASK_TIMEOUT:
    {}break;
  
    case kSTART_STREAMING_ACQUISITION:
      {
        DEBUG_STREAM << "PicoScaleTaskManager::handle_message::kSTART_STREAMING_ACQUISITION" << std::endl;

        //Get message's parameters
        PicoScaleTaskManager::acquisitionParams *acquisitionParams;
        _msg.detach_data<PicoScaleTaskManager::acquisitionParams>(acquisitionParams); 
        PicoScaleTaskManager::acquisitionParams acqParams = *acquisitionParams;

        if (acqParams.acquisitionFrequency != this->_oldFrequency){
          this->_oldFrequency = acqParams.acquisitionFrequency;
          //Configuration while update state..
          this->configureAcquisition(acqParams.acquisitionFrequency);
        }
        //If configuration ok then start streaming acquisition
        if(this->getState() != Tango::FAULT){
            this->setState(Tango::RUNNING);
            this->setStatus("Streaming data launched...");
          if(this->startStreamingPS()){
            this->_numberOfSamplesNeeded = acqParams.sampleQuantity;
            this->_streamingAcquisitionRunning = true;
          }
        }   
      }
    break;

    //- UNHANDLED MSG --------------------
    default:
      DEBUG_STREAM << "PicoScaleTaskManager::handle_message::unhandled msg type received" << std::endl;
    break;
  }
}
//-----------------------------------------------------------------------------------------------
//
//
//				COMANDS from device
//
//
//-----------------------------------------------------------------------------------------------
// ============================================================================
// PicoScaleTaskManager::startAcquisition
// ============================================================================
void PicoScaleTaskManager::startAcquisition(int sampleQuantity, int acquisitionFrequency)
{
  this->_acquisitionOver = false;
  this->setState(Tango::RUNNING);
  this->setStatus( "Acquisition starting up..." );
  
  if (_connected){
    yat::MutexLock gard(this->_data_mutex);
    for (unsigned int i = 0; i < MAX_LENGTH; ++i ){
      this->_dataToReturn.positionsBufferedV1[i] = 0;
      this->_dataToReturn.positionsBufferedV2[i] = 0;
      this->_dataToReturn.positionsBufferedV3[i] = 0;
    } 
    _dataToReturn.sampleQuantityBuffered = 0;
    acquisitionParams acqParams;
    acqParams.sampleQuantity = sampleQuantity;
    acqParams.acquisitionFrequency = acquisitionFrequency;
    //Yat Message 
    yat::Message* msg = 0;
    //Construction of message
    msg = yat::Message::allocate(kSTART_STREAMING_ACQUISITION, DEFAULT_MSG_PRIORITY, false);
    msg->attach_data(acqParams);
    this->post(msg);
  }else {
    this->setState(Tango::FAULT);
    this->setStatus("Couldn't start acquisition! \n Device is not connected!");
  }
}//release data-accessMutex
// ============================================================================
// PicoScaleTaskManager::stopAcquisition
// ============================================================================
void PicoScaleTaskManager::stopAcquisition()
{
  _streamingAcquisitionRunning = false;
  this->stopStreamingPS();
  //Get last buffers events -- Avoid a BUG on Smaract side...
  //If this is not done, a wrong bufferID will be send during next acquisition...
  unsigned int bufferID;
  waitAcqForBufferPS(NONE, MIN_TIMEOUT, &bufferID);//Buffer 0
  waitAcqForBufferPS(NONE, MIN_TIMEOUT, &bufferID);//Buffer 1
}
// ============================================================================
// PicoScaleTaskManager::resetPositions
// ============================================================================
void PicoScaleTaskManager::resetPositions()
{ 
  //Setting Channel 1 to 0
  SA_SI_PropertyKey channel_one_position_property = SA_SI_EPK(SA_PS_CH_POSITION_PROP, CHANNEL_1_Index , NONE);
  checkResultFromAPI(this->setProperty64PS(channel_one_position_property, 0), MODERATE, "While setting SA_PS_CH_POSITION_PROP on channel 1.");
  //Setting Channel 2 to 0
  SA_SI_PropertyKey channel_two_position_property = SA_SI_EPK(SA_PS_CH_POSITION_PROP, CHANNEL_2_Index , NONE);
  checkResultFromAPI(this->setProperty64PS(channel_two_position_property, 0), MODERATE, "While setting SA_PS_CH_POSITION_PROP on channel 2.");
  //Setting Channel 3 to 0
  SA_SI_PropertyKey channel_three_position_property = SA_SI_EPK(SA_PS_CH_POSITION_PROP, CHANNEL_3_Index , NONE);
  checkResultFromAPI(this->setProperty64PS(channel_three_position_property, 0), MODERATE, "While setting SA_PS_CH_POSITION_PROP on channel 3.");
}
// ============================================================================
// PicoScaleTaskManager::getData
// ============================================================================ 
double* PicoScaleTaskManager::getData(int channel)
{
  //Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	//To return selected buffer 
	if (channel == CHANNEL_1)
		return this->_dataToReturn.positionsBufferedV1;
	if (channel == CHANNEL_2)
		return this->_dataToReturn.positionsBufferedV2;
	if (channel == CHANNEL_3)
		return this->_dataToReturn.positionsBufferedV3;
}
// ============================================================================
// PicoScaleTaskManager::getSampleQuantityBuffered
// ============================================================================ 
double PicoScaleTaskManager::getSampleQuantityBuffered()
{
	yat::MutexLock gard(this->_data_mutex);
  return this->_dataToReturn.sampleQuantityBuffered;
}
// ============================================================================
// PicoScaleTaskManager::dataImported
// Data has been imported by device
// If acquisition is over then set state back to standby
// ============================================================================ 
void PicoScaleTaskManager::dataImported()
{
  yat::MutexLock gard(this->_data_mutex);
  this->_state = Tango::STANDBY;
  this->setStatus("Device is in idle state" ); 
}
// ============================================================================
// PicoScaleTaskManager::isAcquisitionOver
// ============================================================================ 
bool PicoScaleTaskManager::isAcquisitionOver(){
  return this->_acquisitionOver;
}
// ============================================================================
// PicoScaleTaskManager::getState
// ============================================================================ 
Tango::DevState PicoScaleTaskManager::getState()
{
  return this->_state;
}
// ============================================================================
// PicoScaleTaskManager::getStatus
// ============================================================================ 
std::string PicoScaleTaskManager::getStatus()
{
  return this->_status;
}
//-----------------------------------------------------------------------------
//
//
//				PS API Access
//
//
//-----------------------------------------------------------------------------

// ============================================================================
// PicoScaleTaskManager::getBufferFromPS
// ============================================================================
void PicoScaleTaskManager::getBufferFromPS(unsigned int bufferID)
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  const SA_SI_DataBuffer *buffer;
  unsigned int result;
  uint32_t seqCounter = 0;
  uint32_t numOfSources = 0;
  uint32_t numOfFrames = 0;
  uint32_t flags = 0;
  bool lastAcquisition = false;
  //To deal with wrong buffer id problem... If the buffer Id is not correct, do nothing and wait for an other one...
  if (checkResultFromAPI(SA_SI_AcquireBuffer(this->_psHandler, bufferID, &buffer), NONE, "While aqcuiring buffer.")){
    checkResultFromAPI(SA_SI_GetBufferInfo(this->_psHandler, bufferID, &seqCounter, &numOfSources, &numOfFrames, &flags), FATAL, "While getting buffer informations.");

    uint8_t dest0[MAX_CHANNEL_BUFFER_SIZE];
    uint8_t dest1[MAX_CHANNEL_BUFFER_SIZE];
    uint8_t dest2[MAX_CHANNEL_BUFFER_SIZE];
    unsigned int channel;
    unsigned int dsourceIndex;
    
    yat::MutexLock gard(this->_data_mutex);
    
    //Last extraction to avoid to many samples...
    if ((numOfFrames + _dataToReturn.sampleQuantityBuffered) > _numberOfSamplesNeeded){
      numOfFrames = _numberOfSamplesNeeded - _dataToReturn.sampleQuantityBuffered;
      lastAcquisition = true;
    }
      
    //GET AND COPY TO CLASS MEMBER BUFFER FROM CHANNEL 1
    checkResultFromAPI(SA_SI_CopyBuffer(this->_psHandler, bufferID, 0, dest0, MAX_CHANNEL_BUFFER_SIZE), FATAL, "While copying buffer for channel 1.");
    this->extractBuffer(dest0, numOfFrames, _dataToReturn.positionsBufferedV1);
    //GET AND COPY TO CLASS MEMBER BUFFER FROM CHANNEL 2
    checkResultFromAPI(SA_SI_CopyBuffer(this->_psHandler, bufferID, 1, dest1, MAX_CHANNEL_BUFFER_SIZE), FATAL, "While copying buffer for channel 2.");
    this->extractBuffer(dest1, numOfFrames, _dataToReturn.positionsBufferedV2);
    //GET AND COPY TO CLASS MEMBER BUFFER FROM CHANNEL 3
    checkResultFromAPI(SA_SI_CopyBuffer(this->_psHandler, bufferID, 2, dest2, MAX_CHANNEL_BUFFER_SIZE), FATAL, "While copying buffer for channel 3.");
    this->extractBuffer(dest2, numOfFrames, _dataToReturn.positionsBufferedV3);

    //UPDATE TOTAL NUMBER OF SAMPLES
    this->_dataToReturn.sampleQuantityBuffered = this->_dataToReturn.sampleQuantityBuffered + numOfFrames;

    //THEN RELEASE BUFFER....      
    checkResultFromAPI(SA_SI_ReleaseBuffer(this->_psHandler, bufferID), FATAL, "While releasing buffer.");
    
    //If it was the last packet then stop acquisition...
    if(lastAcquisition)
      stopAcquisition();
  }
}
// ============================================================================
// PicoScaleTaskManager::initPS
// ============================================================================
void PicoScaleTaskManager::initPS(std::string deviceID)
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  //Try to connect to the intrument with given ID
  const char * charId = deviceID.c_str();
  //not used
  const char *config;
  int result = SA_SI_Open(&_psHandler, charId, "");
  if (result == SA_SI_OK){
    this->_connected = true;
    //Getting full access connection in order to modify specific device properties
    SA_SI_PropertyKey full_access_property = SA_SI_EPK(SA_PS_SYS_FULL_ACCESS_CONNECTION_PROP, NONE, NONE);
    checkResultFromAPI(this->setProperty32PS(full_access_property, SA_SI_ENABLED), MODERATE, "While setting SA_PS_SYS_FULL_ACCESS_CONNECTION_PROP...");
  }
  else {
    this->_connected = false;
  }
}
// ============================================================================
// PicoScaleTaskManager::closePS
// ============================================================================
void PicoScaleTaskManager::closePS()
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  SA_SI_Close(_psHandler);
}
// ============================================================================
// PicoScaleTaskManager::setProperty32PS
// ============================================================================
int PicoScaleTaskManager::setProperty32PS(SA_SI_PropertyKey propertyToSet, int value)
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  return SA_SI_SetProperty_i32(this->_psHandler, propertyToSet, value);
}
// ============================================================================
// PicoScaleTaskManager::setProperty64PS
// ============================================================================
int PicoScaleTaskManager::setProperty64PS(SA_SI_PropertyKey propertyToSet, int value)
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  return SA_SI_SetProperty_i64(this->_psHandler, propertyToSet, value);
}
// ============================================================================
// PicoScaleTaskManager::resetStreamingPS
// ============================================================================
int PicoScaleTaskManager::resetStreamingPS()
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  return SA_SI_ResetStreamingConfiguration(_psHandler);
}
// ============================================================================
// PicoScaleTaskManager::startStreamingPS
// ============================================================================
bool PicoScaleTaskManager::startStreamingPS()
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  SA_SI_PropertyKey streaming_on_prop = SA_SI_EPK(SA_SI_STREAMING_ACTIVE_PROP, 0, 0);
  return checkResultFromAPI(this->setProperty32PS(streaming_on_prop, SA_SI_ENABLED), FATAL, "While starting streaming...");
}
// ============================================================================
// PicoScaleTaskManager::stopStreamingPS
// ============================================================================
void PicoScaleTaskManager::stopStreamingPS()
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  SA_SI_PropertyKey streaming_off_prop = SA_SI_EPK(SA_SI_STREAMING_ACTIVE_PROP, 0, 0);
  checkResultFromAPI(this->setProperty32PS(streaming_off_prop, SA_SI_DISABLED), FATAL, "While stopping streaming...");
}
// ============================================================================
// PicoScaleTaskManager::waitAcqForBufferPS
// ============================================================================
bool PicoScaleTaskManager::waitAcqForBufferPS(int gravity, unsigned int timeOut, unsigned int *bufferID)
{
  //Ps Access
  yat::MutexLock gard(this->_pS_Access);
  SA_SI_Event event;
  event.devEventParameter = SA_SI_STREAMBUFFER_READY_EVENT;
  event.bufferId = 0;
  unsigned int result;
  bool response = checkResultFromAPI(SA_SI_WaitForEvent(this->_psHandler, &event, timeOut), gravity, "While waiting for event : SA_SI_STREAMBUFFER_READY_EVENT.");

  *bufferID = event.bufferId;
  return response;
  }
//-----------------------------------------------------------------------------
//
//
//				TASK use
//
//
//-----------------------------------------------------------------------------
void PicoScaleTaskManager::configurePicoscale(){
  //SET BUFFER TYPE
  SA_SI_PropertyKey interleaved_buffer = SA_SI_EPK(SA_SI_STREAMBUFFERS_INTERLEAVED_PROP,0,0);
  int zero = this->setProperty32PS(interleaved_buffer, SA_SI_DISABLED);
  //DATA SOURCE - ENABLE STREAMING
  //Set number of channels, always use three channels...
  //IndexL =Data source 0 = Postion
  //IndexH = Channel 0 - 2
  SA_SI_PropertyKey streamingEnable_Chanel_1_src_0_PK = SA_SI_EPK(SA_SI_STREAMING_ENABLED_PROP, CHANNEL_1_Index, (SA_SI_ANALOG_RAW_DSOURCE));
  SA_SI_PropertyKey streamingEnable_Chanel_2_src_0_PK = SA_SI_EPK(SA_SI_STREAMING_ENABLED_PROP, CHANNEL_2_Index, (SA_SI_ANALOG_RAW_DSOURCE));
  SA_SI_PropertyKey streamingEnable_Chanel_3_src_0_PK = SA_SI_EPK(SA_SI_STREAMING_ENABLED_PROP, CHANNEL_3_Index, (SA_SI_ANALOG_RAW_DSOURCE));
  checkResultFromAPI(this->setProperty32PS(streamingEnable_Chanel_1_src_0_PK, SA_SI_ENABLED), MODERATE, "While setting SA_SI_STREAMING_ENABLED_PROP on channel 1.");
  checkResultFromAPI(this->setProperty32PS(streamingEnable_Chanel_2_src_0_PK, SA_SI_ENABLED), MODERATE, "While setting SA_SI_STREAMING_ENABLED_PROP on channel 2.");
  checkResultFromAPI(this->setProperty32PS(streamingEnable_Chanel_3_src_0_PK, SA_SI_ENABLED), MODERATE, "While setting SA_SI_STREAMING_ENABLED_PROP on channel 3.");
  // //Disable Compression
  SA_SI_PropertyKey compression_ch0 = SA_SI_EPK(SA_SI_COMPRESSION_MODE_PROP, CHANNEL_1_Index, (SA_SI_ANALOG_RAW_DSOURCE));
  SA_SI_PropertyKey compression_ch1 = SA_SI_EPK(SA_SI_COMPRESSION_MODE_PROP, CHANNEL_2_Index, (SA_SI_ANALOG_RAW_DSOURCE));
  SA_SI_PropertyKey compression_ch2 = SA_SI_EPK(SA_SI_COMPRESSION_MODE_PROP, CHANNEL_3_Index, (SA_SI_ANALOG_RAW_DSOURCE));
  checkResultFromAPI(this->setProperty32PS(compression_ch0, SA_SI_DISABLED), NONE, "While setting SA_SI_COMPRESSION_MODE_PROP on channel 1.");
  checkResultFromAPI(this->setProperty32PS(compression_ch1, SA_SI_DISABLED), NONE, "While setting SA_SI_COMPRESSION_MODE_PROP on channel 2.");
  checkResultFromAPI(this->setProperty32PS(compression_ch2, SA_SI_DISABLED), NONE, "While setting SA_SI_COMPRESSION_MODE_PROP on channel 3.");
  //DIRECT STREAMING
  SA_SI_PropertyKey streaming_mode = SA_SI_EPK(SA_SI_STREAMING_MODE_PROP, 0, 0);
  checkResultFromAPI(this->setProperty32PS(streaming_mode, SA_SI_DIRECT_STREAMING ), NONE, "While setting SA_SI_STREAMING_MODE_PROP to SA_SI_DIRECT_STREAMING");

}
// ============================================================================
// PicoScaleTaskManager::configureAcquisition
// ============================================================================
void PicoScaleTaskManager::configureAcquisition(int acquisitionFrequency)
{

  //FRAME AGREGATION
  SA_SI_PropertyKey frameBufferAggregation = SA_SI_EPK(SA_SI_FRAME_AGGREGATION_PROP, 0,0);
  //TO AVOID TIMEOUT WHILE WAITING FOR BUFFER
  int buffer_size = 1;
  if (acquisitionFrequency <= MAX_BUFFER_SIZE)
    buffer_size = acquisitionFrequency;
  else
    buffer_size = MAX_BUFFER_SIZE;
  //SETTING BUFFER SIZE
  checkResultFromAPI(this->setProperty32PS(frameBufferAggregation, buffer_size), FATAL, "While setting SA_SI_FRAME_AGGREGATION_PROP.");

  // Number of frames that are aggregated in a streambuffer.
  // if set to 0, the streambuffer aggregation will be the same
  // as the SI device's streaming packet aggregation. 
  // -> IndexL/IndexH not used -> 0
  SA_SI_PropertyKey streamBufferAggregation = SA_SI_EPK(SA_SI_STREAMBUFFER_AGGREGATION_PROP,0, 0);
  checkResultFromAPI(this->setProperty32PS(streamBufferAggregation, 0), NONE, "While setting SA_SI_STREAMBUFFER_AGGREGATION_PROP.");
  //FRAME RATE
  SA_SI_PropertyKey streamFrameRate = SA_SI_EPK(SA_SI_FRAME_RATE_PROP, 0, 0);
  checkResultFromAPI(this->setProperty32PS(streamFrameRate, acquisitionFrequency), FATAL, "While setting SA_SI_FRAME_RATE_PROP.");
}
// ============================================================================
// PicoScaleTaskManager::refreshSignalQuality
// ============================================================================
void PicoScaleTaskManager::refreshSignalQuality()
{
  //TODO
}
// ============================================================================
// PicoScaleTaskManager::setState
// ============================================================================
void PicoScaleTaskManager::setState(Tango::DevState state)
{
  this->_state = state;
}
// ============================================================================
// PicoScaleTaskManager::setStatus
// ============================================================================
void PicoScaleTaskManager::setStatus(std::string status)
{
  this->_status = status;
}
// ============================================================================
// PicoScaleTaskManager::extractBuffer
// ============================================================================
void PicoScaleTaskManager::extractBuffer(uint8_t* dest, uint32_t frameNumber, double *positionsBuffered)
{
  uint8_t *localDest = dest;

  for(int j=0; j<frameNumber; j++){
    std::string bit_string = "";
    bool inversVal = false;
    int indiceInPositionTable = this->_dataToReturn.sampleQuantityBuffered + j;
    
    //This byte determine the complement value and the negative values apparently...
    if( (short)dest[(FRAME_SIZE * (j+1)) - 1] == 255)
      inversVal = true;
    
    for(int i = (FRAME_SIZE * (j+1)); i >= ((j * FRAME_SIZE)); --i){
      //Drop last packet (trash..) 
      if (i < (FRAME_SIZE * (j+1))){
        unsigned short short1;
        if(inversVal)
          short1 = 255 - (short)dest[i];      
        else
          short1 = (short)dest[i];
        bitset<8> bitset(short1); 
        bit_string = bit_string + bitset.to_string<char,std::string::traits_type,std::string::allocator_type>();
      }  
    }
    char *ptr;
    long long  StrolResult;
    
    StrolResult = _strtoi64(bit_string.c_str(), &ptr, 2);
    
    if(inversVal)
      positionsBuffered[indiceInPositionTable] = (double) StrolResult * (-1);
    else
      positionsBuffered[indiceInPositionTable] = (double) StrolResult;
  }
}
// ============================================================================
// PicoScaleTaskManager::checkQuantityBuffered
// ============================================================================
void PicoScaleTaskManager::checkQuantityBuffered()
{
  //To check if we have enough
  yat::MutexLock gard(this->_data_mutex);
  if (this->_dataToReturn.sampleQuantityBuffered >= _numberOfSamplesNeeded){
    stopAcquisition();
  }
}
// ============================================================================
// PicoScaleTaskManager::checkResultFromAPI
// ============================================================================
bool PicoScaleTaskManager::checkResultFromAPI(int result, int severity, std::string description)
{
  //Trace each call
  std::string debugString = getMessage(result) + "\n" + description;
  DEBUG_STREAM<<debugString<<endl;
  std::string status = "";
  //if not connected
  if (result == SA_SI_COMMUNICATION_ERROR ||
      result == SA_SI_NOT_INITIALIZED_ERROR){
        status = getStatus();
        status = "Device is not connected anymore...\n" +  getMessage (result) + "\n Desc : " + description;
        setStatus (status);
        setState (Tango::FAULT);
        //In case acquisition is running
        if(this->_streamingAcquisitionRunning)
          stopAcquisition();
        return FAILURE;
      }
  else if (result != SA_SI_OK){
    switch(severity) {
      case NONE:{
        std::string errorString = getMessage(result) + "\n" + description;
        ERROR_STREAM<<errorString<<endl;
        return FAILURE;
      }break;           
      case MODERATE:{
        status = getStatus();
        status = status + "\n" +  getMessage (result) + "\n Desc : " + description;
        setStatus (status);
        setState (Tango::ALARM);
        return FAILURE;
      }break;           
      case FATAL:{
        status = getStatus();
        status = status + "\n" +  getMessage (result) + "\n Desc : " + description;
        setStatus (status);
        setState (Tango::FAULT);
        //In case acquisition is running
        if(this->_streamingAcquisitionRunning)
          stopAcquisition();
        return FAILURE;
      }break;           
       default:  
       return FAILURE;
       break;       
    }
  }
  else
    return SUCCESS;
}
// ============================================================================
// PicoScaleTaskManager::getMessage
// ============================================================================
std::string PicoScaleTaskManager::getMessage(int code)
{
  switch(code) {
  case SA_SI_OK:           
      return "SA_SI_OK";
  case SA_SI_UNKNOWN_COMMAND_ERROR:        
      return "SA_SI_UNKNOWN_COMMAND_ERROR";
  case SA_SI_PACKET_SIZE_ERROR:      
      return "SA_SI_PACKET_SIZE_ERROR";
  case SA_SI_TIMEOUT_ERROR: 
      return "SA_SI_TIMEOUT_ERROR";
  case SA_SI_PROTOCOL_ERROR:  
      return "SA_SI_PROTOCOL_ERROR";
  case SA_SI_BUFFER_UNDERFLOW_ERROR: 
      return "SA_SI_BUFFER_UNDERFLOW_ERROR";
  case SA_SI_BUFFER_OVERFLOW_ERROR:      
      return "SA_SI_BUFFER_OVERFLOW_ERROR";
  case SA_SI_INVALID_PACKET_ERROR:     
      return "SA_SI_INVALID_PACKET_ERROR";
  case SA_SI_INVALID_STREAM_PACKET_ERROR:     
      return "SA_SI_INVALID_STREAM_PACKET_ERROR";
  case SA_SI_INVALID_PROPERTY_ERROR:     
    return "SA_SI_INVALID_PROPERTY_ERROR";
  case SA_SI_INVALID_PARAMETER_ERROR:     
    return "SA_SI_INVALID_PARAMETER_ERROR";
  case SA_SI_INVALID_CHANNEL_INDEX_ERROR:     
    return "SA_SI_INVALID_CHANNEL_INDEX_ERROR";
  case SA_SI_INVALID_DSOURCE_INDEX_ERROR:     
    return "SA_SI_INVALID_DSOURCE_INDEX_ERROR";
  case SA_SI_INVALID_DATA_TYPE_ERROR:     
    return "SA_SI_INVALID_DATA_TYPE_ERROR";
  case SA_SI_PERMISSION_DENIED_ERROR:     
    return "SA_SI_PERMISSION_DENIED_ERROR";
  case SA_SI_NO_DATA_SOURCES_ENABLED_ERROR:     
    return "SA_SI_NO_DATA_SOURCES_ENABLED_ERROR";
  case SA_SI_STREAMING_ACTIVE_ERROR:     
    return "SA_SI_STREAMING_ACTIVE_ERROR";
  case SA_SI_DATA_SOURCE_NOT_STREAMABLE_ERROR:     
    return "SA_SI_DATA_SOURCE_NOT_STREAMABLE_ERROR";
  case SA_SI_UNKNOWN_DATA_OBJECT_ERROR:     
    return "SA_SI_UNKNOWN_DATA_OBJECT_ERROR";
  case SA_SI_COMMAND_NOT_PROCESSABLE_ERROR:     
    return "SA_SI_COMMAND_NOT_PROCESSABLE_ERROR";
  case SA_SI_FEATURE_NOT_SUPPORTED_ERROR:     
    return "SA_SI_FEATURE_NOT_SUPPORTED_ERROR";
  case SA_SI_OTHER_ERROR:     
    return "SA_SI_OTHER_ERROR";
  case SA_PS_REQUEST_DENIED_ERROR:           
      return "SA_PS_REQUEST_DENIED_ERROR";
  case SA_PS_INTERNAL_COMMUNICATION_ERROR:           
      return "SA_PS_INTERNAL_COMMUNICATION_ERROR";
  case SA_PS_NO_FULL_ACCESS_ERROR:           
      return "SA_PS_NO_FULL_ACCESS_ERROR";
  case SA_PS_WORKING_RANGE_NOT_SET_ERROR:           
      return "SA_PS_WORKING_RANGE_NOT_SET_ERROR";
  case SA_SI_DEVICE_LIMIT_ERROR:           
      return "SA_SI_DEVICE_LIMIT_ERROR";
  case SA_SI_INVALID_LOCATOR_ERROR:           
      return "SA_SI_INVALID_LOCATOR_ERROR";
  case SA_SI_INITIALIZATION_ERROR:           
      return "SA_SI_INITIALIZATION_ERROR";
  case SA_SI_NOT_INITIALIZED_ERROR:           
      return "SA_SI_NOT_INITIALIZED_ERROR";
  case SA_SI_COMMUNICATION_ERROR:           
      return "SA_SI_COMMUNICATION_ERROR";
  case SA_SI_QUERYBUFFER_SIZE_ERROR:           
      return "SA_SI_QUERYBUFFER_SIZE_ERROR";
  case SA_SI_INVALID_HANDLE_ERROR:           
      return "SA_SI_INVALID_HANDLE_ERROR";
  case SA_SI_DATA_SOURCE_ENABLED_ERROR:           
      return "SA_SI_DATA_SOURCE_ENABLED_ERROR";
  case SA_SI_INVALID_STREAMBUFFER_ID_ERROR:           
      return "SA_SI_INVALID_STREAMBUFFER_ID_ERROR";
  case SA_SI_STREAM_SEQUENCE_ERROR:           
      return "SA_SI_STREAM_SEQUENCE_ERROR";
  case SA_SI_NO_DATABUFFER_AVAILABLE_ERROR:           
      return "SA_SI_NO_DATABUFFER_AVAILABLE_ERROR";
  case SA_SI_STREAMBUFFER_NOT_ACQUIRED_ERROR:           
      return "SA_SI_STREAMBUFFER_NOT_ACQUIRED_ERROR";
  case SA_SI_UNEXPECTED_PACKET_RECEIVED_ERROR:           
      return "SA_SI_UNEXPECTED_PACKET_RECEIVED_ERROR";
  case SA_SI_CANCELED_ERROR:           
      return "SA_SI_CANCELED_ERROR";
  case SA_SI_BUFFER_INTERLEAVING_ERROR:           
      return "SA_SI_BUFFER_INTERLEAVING_ERROR";
  case SA_SI_DRIVER_ERROR:           
      return "SA_SI_DRIVER_ERROR";
  case SA_SI_DATA_OBJECT_BUSY_ERROR:           
      return "SA_SI_DATA_OBJECT_BUSY_ERROR";
  default:               
      return "Unknown error code";
  }
 }
























