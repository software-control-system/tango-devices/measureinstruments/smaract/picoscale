//=============================================================================
//
// file :         PicoScaleInterferometerClass.h
//
// description :  Include for the PicoScaleInterferometerClass root class.
//                This class is the singleton class for
//                the PicoScaleInterferometer device class.
//                It contains all properties and methods which the 
//                PicoScaleInterferometer requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source:  $
// $Log:  $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _PicoScaleInterferometerCLASS_H
#define _PicoScaleInterferometerCLASS_H

#include <tango.h>
#include <PicoScaleInterferometer.h>


namespace PicoScaleInterferometer_ns
{//=====================================
//	Define classes for attributes
//=====================================
class V3Attrib: public Tango::SpectrumAttr
{
public:
	V3Attrib():SpectrumAttr("V3", Tango::DEV_DOUBLE, Tango::READ, 100000) {};
	~V3Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_V3(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_V3_allowed(ty);}
};

class V2Attrib: public Tango::SpectrumAttr
{
public:
	V2Attrib():SpectrumAttr("V2", Tango::DEV_DOUBLE, Tango::READ, 100000) {};
	~V2Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_V2(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_V2_allowed(ty);}
};

class V1Attrib: public Tango::SpectrumAttr
{
public:
	V1Attrib():SpectrumAttr("V1", Tango::DEV_DOUBLE, Tango::READ, 100000) {};
	~V1Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_V1(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_V1_allowed(ty);}
};

class v3StandardDeviationAttrib: public Tango::Attr
{
public:
	v3StandardDeviationAttrib():Attr("v3StandardDeviation", Tango::DEV_DOUBLE, Tango::READ) {};
	~v3StandardDeviationAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_v3StandardDeviation(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_v3StandardDeviation_allowed(ty);}
};

class v2StandardDeviationAttrib: public Tango::Attr
{
public:
	v2StandardDeviationAttrib():Attr("v2StandardDeviation", Tango::DEV_DOUBLE, Tango::READ) {};
	~v2StandardDeviationAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_v2StandardDeviation(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_v2StandardDeviation_allowed(ty);}
};

class v1StandardDeviationAttrib: public Tango::Attr
{
public:
	v1StandardDeviationAttrib():Attr("v1StandardDeviation", Tango::DEV_DOUBLE, Tango::READ) {};
	~v1StandardDeviationAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_v1StandardDeviation(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_v1StandardDeviation_allowed(ty);}
};

class sampleTimeAttrib: public Tango::Attr
{
public:
	sampleTimeAttrib():Attr("sampleTime", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~sampleTimeAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_sampleTime(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->write_sampleTime(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_sampleTime_allowed(ty);}
};

class samplesQuantityBufferedAttrib: public Tango::Attr
{
public:
	samplesQuantityBufferedAttrib():Attr("samplesQuantityBuffered", Tango::DEV_DOUBLE, Tango::READ) {};
	~samplesQuantityBufferedAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_samplesQuantityBuffered(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_samplesQuantityBuffered_allowed(ty);}
};

class integrationTimeAttrib: public Tango::Attr
{
public:
	integrationTimeAttrib():Attr("integrationTime", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~integrationTimeAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_integrationTime(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->write_integrationTime(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_integrationTime_allowed(ty);}
};

class averageV3Attrib: public Tango::Attr
{
public:
	averageV3Attrib():Attr("averageV3", Tango::DEV_DOUBLE, Tango::READ) {};
	~averageV3Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_averageV3(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_averageV3_allowed(ty);}
};

class averageV2Attrib: public Tango::Attr
{
public:
	averageV2Attrib():Attr("averageV2", Tango::DEV_DOUBLE, Tango::READ) {};
	~averageV2Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_averageV2(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_averageV2_allowed(ty);}
};

class averageV1Attrib: public Tango::Attr
{
public:
	averageV1Attrib():Attr("averageV1", Tango::DEV_DOUBLE, Tango::READ) {};
	~averageV1Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PicoScaleInterferometer *>(dev))->read_averageV1(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_averageV1_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class ResetAllAxesCmd : public Tango::Command
{
public:
	ResetAllAxesCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ResetAllAxesCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ResetAllAxesCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_ResetAllAxes_allowed(any);}
};



class StopCmd : public Tango::Command
{
public:
	StopCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StopCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StopCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_Stop_allowed(any);}
};



class StartCmd : public Tango::Command
{
public:
	StartCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StartCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StartCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PicoScaleInterferometer *>(dev))->is_Start_allowed(any);}
};



//
// The PicoScaleInterferometerClass singleton definition
//

class
#ifdef WIN32
	__declspec(dllexport)
#endif
	PicoScaleInterferometerClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static PicoScaleInterferometerClass *init(const char *);
	static PicoScaleInterferometerClass *instance();
	~PicoScaleInterferometerClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	PicoScaleInterferometerClass(string &);
	static PicoScaleInterferometerClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace PicoScaleInterferometer_ns

#endif // _PICOSCALEINTERFEROMETERCLASS_H
